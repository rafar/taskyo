

### Config
- create .env from .env.example
- set mysql as database in .env
- run migration

### Commands to run in project directory in bash
- `composer install`
- `php artisan key:generate`
- `php artisan jwt:secret`
- `php artisan migrate:fresh`