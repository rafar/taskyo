<?php

namespace App;

use App\Repositories\BoardListQueryRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\TaskTimer
 *
 * @mixin \Eloquent
 * @property int $id
 * @property int $task_id
 * @property string $start
 * @property string|null $stop
 * @property float $hours
 * @property string $date
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaskTimer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaskTimer whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaskTimer whereHours($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaskTimer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaskTimer whereStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaskTimer whereStop($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaskTimer whereTaskId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaskTimer whereUpdatedAt($value)
 * @property-read \App\Task $task
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaskTimer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaskTimer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaskTimer query()
 */
class TaskTimer extends Model
{
    protected $fillable = ['stop', 'hours'];

    protected $dates = [
      'start', 'stop', 'date'
    ];

    protected $casts = [
      'date'=> 'date:Y-m-d'
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('user_id', function (Builder $builder) {
            $builder->whereIn('task_id',
              Task::whereIn('board_list_id',
                (new BoardListQueryRepository())->getAll()->pluck('id')
                  ->toArray())->pluck('id')
                ->toArray());
        });
    }

    public function task()
    {
        return $this->belongsTo('App\Task');
    }
}
