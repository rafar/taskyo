<?php

namespace App\Repositories;

use App\BoardList;
use App\Services\cache\CacheConsts;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Cache;

class BoardListQueryRepository
{
    /**
     * @return Collection|BoardList[]
     */
    public function getAll(): Collection
    {
        return Cache::remember(
          CacheConsts::getCacheKeyForId(CacheConsts::CACHE_ALL_BOARDS_LISTS, auth()->id()),
          CacheConsts::WEEK,
          function () {
              return BoardList::all();
          });
    }
}