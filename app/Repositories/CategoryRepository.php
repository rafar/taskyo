<?php

namespace App\Repositories;

use App\Category;
use App\Http\Requests\categories\CategoryRequest;

class CategoryRepository
{
    /**
     * @param CategoryRequest $data
     * @return Category
     */
    public static function create(CategoryRequest $data): Category
    {
        $category = new Category();
        $category->name = $data->name;
        $category->color = $data->color;
        $category->user_id = \Auth::id();
        $category->save();
        return $category;
    }

    public static function update(Category $category, CategoryRequest $data): Category
    {
        $category->update($data->only('name', 'color'));
        return $category;
    }

}