<?php

namespace App\Repositories;


use App\Board;
use App\BoardList;
use App\Http\Requests\board\BoardStoreRequest;
use App\Services\board\statistics\BoardStatistics;
use App\Task;
use App\TaskTimer;
use Illuminate\Support\Facades\DB;

class BoardRepository
{
    /**
     * @param Board $board
     * @return array
     * should be refactored
     * @deprecated
     * @see BoardStatistics::getSumOfHoursByDaysByBoardId
     */
    public static function getSumOfHoursByDays(Board $board): array
    {
        $taskTimers = TaskTimer
          ::selectRaw('date, SUM(hours) AS hours')
          ->whereIn('task_id', Task
            ::whereIn('board_list_id', BoardList
              ::whereBoardId($board->id)->pluck('id')->toArray()
            )->pluck('id')->toArray()
          )->groupBy('date')->orderByDesc('date')->get();
        return $taskTimers->toArray();
    }
}