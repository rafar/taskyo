<?php
/**
 * Created by PhpStorm.
 * User: dariusz
 * Date: 27.05.18
 * Time: 14:36
 */

namespace App\Repositories;


use App\Http\Requests\timers\TaskTimerRequest;
use App\TaskTimer;
use Carbon\Carbon;

class TaskTimerRepository
{
    public static function startTimer(TaskTimerRequest $data): TaskTimer
    {
        $taskTimer = new TaskTimer();
        $taskTimer->task_id = $data->taskId;
        $taskTimer->date = Carbon::now()->toDateString();
        $taskTimer->start = Carbon::parse($data->start);
        $taskTimer->save();
        return $taskTimer;
    }

    public static function stopTimer(TaskTimer $timer, $stop): TaskTimer
    {
        $stop = Carbon::parse($stop)->toDateTimeString();
        $hours = Carbon::parse($stop)->diffInSeconds(Carbon::parse($timer->start)) / 3600;
        if ($hours < 1/240) { //less than quarter minute
            $timer->delete();
            return $timer;
        }
        $timer->update(['stop' => $stop, 'hours' => $hours]);
        return $timer;
    }
}