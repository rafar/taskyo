<?php

namespace App\Http\Controllers;

use App\BoardList;
use App\Task;
use App\TaskTimer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $task = new Task();

        $task->name = $request->task['name'];
        $task->status = 'PENDING';
        $task->priority = (int)$request->task['priority'];
        $task->board_list_id = (int)$request->task['board_list_id'];

        BoardList::findOrFail($task->board_list_id);

        $task->save();
        return Response::json(array('success' => true, 'task' => $task), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Task $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Task $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        BoardList::findOrFail($task->board_list_id);
        $task->update(['priority' => $request->priority, 'status'=>$request->status, 'name'=>$request->name]);
        return response(['task' => $task], 200);
    }

    public function updateAll(Request $request)
    {
        $tasks = Task::where('board_list_id', $request->board_list_id)->get();
        BoardList::findOrFail($request->board_list_id);

        //loop for update order
        /** @var Task $task */
        foreach ($tasks as $task) {
            $task->timestamps = false;
            foreach ($request->tasks as $index => $taskFrontEnd) {
                if ($taskFrontEnd['id'] == $task->id) {
                    if ($task->order != $index) {
                        $task->update(['order' => $index]);
                    }
                    break;
                }
            }
        }

        //loop to search for tasks that changed lists
        if (count($tasks) != count($request->tasks)) {
            foreach ($request->tasks as $taskFrontEnd) {
                if ($taskFrontEnd['board_list_id'] != $request->board_list_id) {
                    $taskToMove = Task::findOrFail($taskFrontEnd['id']);
                    BoardList::findOrFail($taskFrontEnd['board_list_id']);
                    $taskToMove->update(['board_list_id' => $request->board_list_id]);
                }
            }
        }
        return response('Update Successful.', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        TaskTimer::whereTaskId($task->id)->delete();
        $task->delete();
        return response()->json();
    }

    /**
     * @param Task $task
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBoardIdByTaskId(Task $task) {
        $boardList = BoardList::findOrFail($task->board_list_id);
        return  Response::json(['boardId' => $boardList->board_id]);
    }
}
