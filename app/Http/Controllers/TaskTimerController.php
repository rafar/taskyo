<?php

namespace App\Http\Controllers;

use App\Http\Requests\timers\TaskTimerRequest;
use App\Http\Resources\TaskTimerResource;
use App\Repositories\TaskTimerRepository;
use App\Task;
use App\TaskTimer;
use Illuminate\Http\Request;

class TaskTimerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param TaskTimerRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(TaskTimerRequest $request)
    {
        $taskTimer = TaskTimerRepository::startTimer($request);
        return \Response::json($taskTimer);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TaskTimer $taskTimer
     * @return \Illuminate\Http\Response
     */
    public function edit(TaskTimer $taskTimer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\TaskTimer $taskTimer
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, TaskTimer $taskTimer)
    {
        $taskId = $taskTimer->task_id;
        $taskTimer = TaskTimerRepository::stopTimer($taskTimer, $request->stop);
        $hours = TaskTimer::whereTaskId($taskId)->sum('hours');
        return \Response::json(['taskTimer' => $taskTimer, 'sumOfTaskHours' => $hours]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TaskTimer $taskTimer
     * @throws \Exception
     */
    public function destroy(TaskTimer $taskTimer)
    {
        $taskTimer->delete();
    }

    /**
     * @param Task $task
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getByTaskId(Task $task)
    {
        return TaskTimerResource::collection(TaskTimer::whereTaskId($task->id)->orderByDesc('id')->paginate(99));
    }
}
