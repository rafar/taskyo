<?php

namespace App\Http\Controllers;

use App\Board;
use App\Http\Requests\board\BoardStoreRequest;
use App\Http\Resources\BoardResource;
use App\Repositories\BoardCommandRepository;
use App\Repositories\BoardRepository;
use App\Services\board\statistics\BoardStatistics;
use Illuminate\Support\Facades\Response;

class BoardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return BoardResource::collection(Board::orderByDesc('id')->paginate(50));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param BoardStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(BoardStoreRequest $request)
    {
        $board = (new BoardCommandRepository())->create($request);
        $board->load('category');
        return \Response::json(array('success' => true, 'board' => $board), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param BoardStoreRequest $request
     * @param  \App\Board $board
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(BoardStoreRequest $request, Board $board)
    {
        $board = (new BoardCommandRepository())->update($board, $request);
        return Response::json(array('success' => true, 'board' => $board), 200);
    }

    /**
     * @param  \App\Board $board
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Board $board)
    {
        try {
            $board->delete();
        } catch (\Exception $e) {
            return Response::json(array('success' => false, 'board' => $board), 500);
        }
        return Response::json(array('success' => true, 'board' => $board), 200);
    }

    public function statistics(Board $board)
    {
        $statistics = new BoardStatistics();
        $data = [
          'sumOfHoursByDays' => BoardRepository::getSumOfHoursByDays($board),
          'tasksByDays' => $statistics->getTasksByDays($board->id),
        ];
        return Response::json($data);
    }
}
