<?php

namespace App;

use App\Scopes\UserIdScope;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Board
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\BoardList[] $lists
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Board whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Board whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Board whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Board whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $user_id
 * @property int|null $archived
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Board newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Board newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Board query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Board whereArchived($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Board whereUserId($value)
 * @property int|null $category_id
 * @property-read \App\Category|null $category
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Board whereCategoryId($value)
 */
class Board extends Model
{
    protected $fillable = ['name', 'order', 'archived', 'category_id'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new UserIdScope);
    }

    public function lists()
    {
        return $this->hasMany('App\BoardList');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }
}
