<?php

namespace App;

use App\Repositories\BoardQueryRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\BoardList
 *
 * @property int $id
 * @property int $board_id
 * @property string $name
 * @property int $order
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Board $board
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Task[] $tasks
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BoardList whereBoardId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BoardList whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BoardList whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BoardList whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BoardList whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BoardList whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BoardList newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BoardList newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BoardList query()
 * @property int|null $archived
 * @property string|null $color
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BoardList whereArchived($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BoardList whereColor($value)
 */
class BoardList extends Model
{
    protected $fillable = ['name', 'archived', 'color', 'order'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('board_list_id', function (Builder $builder) {
            $builder->whereIn('board_id',
              (new BoardQueryRepository())->getAll()->pluck('id')
                  ->toArray());
        });
    }

    public function board()
    {
        return $this->belongsTo('App\Board');
    }

    public function tasks()
    {
        return $this->hasMany('App\Task')->orderByDesc('status')->orderBy('priority')->orderBy('order')->orderBy('id');
    }
}
