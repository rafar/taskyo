<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('board_list_id')->index()->unsigned();
            $table->string('name');
            $table->enum('status', ['DONE', 'PENDING'])->default('PENDING');
            $table->tinyInteger('priority')->unsigned()->default(2);
            $table->tinyInteger('order')->unsigned()->default(0);
            $table->date('deadline')->nullable();
            $table->timestamps();
            $table->foreign('board_list_id')->references('id')->on('board_lists');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
