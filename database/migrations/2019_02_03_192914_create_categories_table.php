<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->index()->unsigned();
            $table->string('name');
            $table->string('color');
            $table->boolean('archived')->nullable()->index();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('boards', function (Blueprint $table) {
            $table->integer('category_id')->after('user_id')->nullable()->default(null)->index()->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
        Schema::table('boards', function (Blueprint $table) {
            $table->dropColumn('category_id');
        });
    }
}
