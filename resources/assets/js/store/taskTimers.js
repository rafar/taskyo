const state = {
    taskTimers: []
};

const getters = {
    timers: (state) => {
        return state.taskTimers;
    },
    timerByTaskId: (state) => {
        return taskId => state.taskTimers.filter(taskTimer => {
            return taskTimer.taskId == taskId
        })[0];
    },
    timersByTaskId: (state) => {
        return taskId => {
            if (!state.taskTimers) {
                return null;
            }
            return state.taskTimers.filter(timer => {
                return timer.taskId = taskId;
            })
        }
    }
};

const mutations = {
    startTimer: (state, task) => {
        state.taskTimers.push({
            taskId: task.id,
            id: task.timerId,
            taskName: task.name,
            start: new Date()
        })
    },
    stopTimer: (state, timerId) => {
        for (let index in state.taskTimers) {
            let timer = state.taskTimers[index];
            if (timer.id == timerId) {
                state.taskTimers.splice(index, 1);
            }
        }
    },
    setFetchedTaskTimers: (state, timers) => {
        for (let index in timers) {
            let timer = timers[index];
            state.taskTimers.push({
                'id': timer.id,
                'taskId': timer.task_id,
                'taskName': timer.task.name,
                'start': new Date(timer.start)
            })
        }
    }
};

const actions = {
    startTimer(context, task) {
        return axios.post('/api/taskTimers', {taskId: task.id, start: new Date().toLocaleString()})
            .then(r => r.data)
            .then(response => {
                task.timerId = response.id;
                context.commit('startTimer', task);
                return response;
            })
            .catch(e => {
                console.log(e)
            });
    },
    stopTimer(context, timerId) {
        return axios.put('/api/taskTimers/' + timerId, {stop: new Date().toLocaleString()})
            .then(r => r.data)
            .then(response => {
                context.commit('stopTimer', timerId);
                return response
            })
            .catch(e => {
                console.log(e)
            });
    }
};

export default {
    state, mutations, getters, actions
}