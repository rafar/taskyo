import Vue from "vue";

const state = {
    boards: [],
    activeBoardId: 0,
};

const getters = {
    boards: (state) => {
        return state.boards;
    },
    activeBoard: (state) => {
        for (let index in state.boards) {
            let board = state.boards[index];
            if (board.id == state.activeBoardId) {
                board.index = index;
                return board;
            }
        }
        return null;
    }
};

const mutations = {
    setActiveBoardId: (state, boardId) => {
        state.activeBoardId = boardId;
    },
    addBoard: (state, payload) => {
        state.boards.push(payload)
    },
    setFetchedBoards(state, boards) {
        state.boards = boards;
    },
    deleteBoard: (state, boardId) => {
        for (let index in state.boards) {
            let board = state.boards[index];
            if (board.id == boardId) {
                state.boards.splice(index, 1);
            }
        }
    },
    setFetchedLists(state, listsObject) {
        Vue.set(state.boards[listsObject.board.index], 'lists', listsObject.lists);
    },
    addList: (state, listsObject) => {
        state.boards[listsObject.board.index].lists.push(listsObject.list)
    },
    updateTasks(state, payload) {
        payload.list.tasks = payload.tasks;
    },
    toggleTaskPriority(state, payload) {
        payload.task.priority = payload.priority;
    },
    deleteTaskById: (state, taskPayload) => {
        const boardId = taskPayload.boardId;
        const listId = taskPayload.listId;
        const taskId = taskPayload.id;
        let board = state.boards[_.findIndex(state.boards, {id: boardId})];
        let list = board.lists[_.findIndex(board.lists, {id: listId})];
        let taskIndex = list.tasks[_.findIndex(list.tasks, {id: taskId})];
        list.tasks.splice(taskIndex, 1);
    }
};

const actions = {
    addBoard(context, board) {
        return axios.post('/api/boards', board)
            .then(r => r.data)
            .then(response => {
                board = response.board;
                context.commit('addBoard', board);
            });
    },
    updateBoard(context, board) {
        return axios.put('/api/boards/' + board.id, board)
            .then(r => r.data)
            .then(response => {
            });
    },
    deleteBoard(context, boardId) {
        axios.delete('/api/boards/' + boardId)
            .then(() => {
                context.commit('deleteBoard', boardId);
            })
            .catch(e => {
                this.errors.push(e)
            })
    },
    fetchStartData(context) {
        if (state.boards.length) {
            return false;
        }
        return axios
            .get('/api/startData')
            .then(r => r.data)
            .then(startData => {
                context.commit('setFetchedBoards', startData.boards);
                context.commit('setFetchedTaskTimers', startData.taskTimers);
                context.commit('setFetchedCategories', startData.categories);
            })
    },
    fetchLists(context, force = false) {
        let board = context.getters.activeBoard;
        if (typeof board.lists !== 'undefined' && !force) {
            return Promise.resolve();
        }

        return axios
            .get('/api/lists/byBoard/' + board.id)
            .then(r => r.data.data)
            .then(lists => {
                context.commit('setFetchedLists', {lists: lists, board: board})
            })
    },
    addList(context, list) {
        let board = context.getters.activeBoard;
        return axios.post('/api/boardLists', {list: list})
            .then(r => r.data)
            .then(response => {
                list.id = response.id;
                context.commit('addList', {list: list, board: board});
            })
            .catch(e => {
                console.log(e)
            });
    },
    updateTasks(context, payload) {
        axios.put('/api/tasksDraggable', {tasks: payload.tasks, 'board_list_id': payload.list.id})
            .then(r => r.data)
            .then(response => {
                payload.task = response.task;
                context.commit('updateTasks', payload);
            })
            .catch(e => {
                console.log(e)
            });
    },
    setActiveBoardIdByTaskId(context, taskId) {
        return axios.get('/api/tasks/getBoardIdByTaskId/' + taskId)
            .then(response => response.data)
            .then(response => {
                context.commit('setActiveBoardId', response.boardId);
            }).catch(e => {
                console.log(e)
            });
    },
    deleteTask(context, task) {
        return axios.delete('/api/tasks/' + task.id)
            .then(() => {
                context.commit('deleteTaskById', task)
            })
            .catch(e => {
                console.log(e);
            });
    },
};

export default {
    state, mutations, actions, getters
}