const state = {
    favicon: "/favicon_idle.ico"
};

const getters = {
    favicon: (state) => {
        return state.favicon;
    },
};

const mutations = {
    setIdleFavicon: (state) => {
        state.favicon = '/favicon_idle.ico';
        document.querySelector("link[rel*='icon']").href = state.favicon;
    },
    setWorkingFavicon: (state) => {
        state.favicon = '/favicon_working.ico';
        document.querySelector("link[rel*='icon']").href = state.favicon;
    }
};

export default {
    state, mutations, getters
}