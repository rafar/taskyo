import Vue from "vue";

const state = {
    categories: [],
    activeCategoryId: 0,
};

const getters = {
    categories: (state) => {
        return state.categories;
    },
};

const mutations = {
    setFetchedCategories(state, categories) {
        state.categories = categories;
    },
    addCategory: (state, payload) => {
        state.categories.push(payload)
    },
    updateCategory: (state, payload) => {

    }
};

const actions = {
    addCategory(context, category) {
        return axios.post('/api/categories', category)
            .then(r => r.data)
            .then(response => {
                category.id = response.id;
                context.commit('addCategory', category);
            });
    },
    updateCategory(context, category) {
        return axios.put('/api/categories/' + category.id, category)
            .then(r => r.data)
            .then(response => {
                category.id = response.id;
                context.commit('updateCategory', category);
            });
    },
};

export default {
    state, mutations, actions, getters
}