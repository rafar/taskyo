require('./bootstrap');

import Vue from 'vue';
import VueRouter from 'vue-router'
import {routes} from './routes'
import {store} from './store/store'
import 'vue-awesome/icons/clock'
import 'vue-awesome/icons/trash'
import 'vue-awesome/icons/cog'
import 'vue-awesome/icons/check-circle'
import Icon from 'vue-awesome/components/Icon'
import Snotify from 'vue-snotify';
Vue.use(VueRouter);
Vue.use(Snotify, {
    toast: {
        timeout: 5000,
        showProgressBar: true,
    },
    config: {
        timeout: 5000,
        showProgressBar: true,
    },
});

const router = new VueRouter({
    routes,
    mode: 'history',
    scrollBehavior(to, from, savedPosition) {
        if (savedPosition) {
            return savedPosition;
        }
        if (to.hash) {
            return {selector: to.hash}
        }
    }
});
Vue.component('icon', Icon);
Vue.component('app-navigation', require('./components/common/Navigation'));
Vue.component('vue-simple-spinner', require('vue-simple-spinner'));
Vue.filter('formatHours', function (hoursDecimal) {
    let hours = Math.trunc(hoursDecimal);
    let minutes = Math.trunc(hoursDecimal * 60 % 60);
    let seconds = Math.trunc(hoursDecimal * 3600);
    return hours + ':' + ("0" + minutes).slice(-2) + ':' + ("0" + seconds).slice(-2)
});
axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('token');
import DefaultComponent from './components/common/DefaultComponent';
const app = new Vue({
    el: '#app',
    store,
    router,
    components: { DefaultComponent },
    render: h => h(DefaultComponent)
});
