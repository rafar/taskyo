const helpers = {};
helpers.dateDiffToHoursMinutesSeconds = function (dateDiff) {
    if (dateDiff < 0) {
        return '0';
    }
    let diffDays = Math.floor(dateDiff / 86400000); // days
    let diffHrs = Math.floor((dateDiff % 86400000) / 3600000); // hours
    let diffMins = Math.floor(((dateDiff % 86400000) % 3600000) / 60000); // minutes
    let diffSecs = Math.round((((dateDiff % 86400000) % 3600000) % 60000) / 1000); // seconds
    let dateString = [];
    if (diffDays) {
        dateString.push(diffDays+'d ');
    }
    if (diffHrs) {
        dateString.push(diffHrs);
    }
    if (diffMins || diffHrs) {
        dateString.push(helpers.toTwoDigit(diffMins));
    }
    dateString.push(helpers.toTwoDigit(diffSecs));
    return dateString.join(':');
};

helpers.toTwoDigit = function (number) {
    return ("0" + number).slice(-2);
};

helpers.toHoursMinutesSeconds = d => {
    return ("00" + d.getHours()).slice(-2) + ":" +
        ("00" + d.getMinutes()).slice(-2) + ":" +
        ("00" + d.getSeconds()).slice(-2);
};
export default helpers;