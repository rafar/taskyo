let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
    .extract([
        'axios',
        'vue',
        'vue-router',
        'bootstrap',
        'jquery',
        'lodash',
        'vuex',
        'vue-awesome/icons/clock',
        'vue-awesome/icons/trash',
        'vue-awesome/icons/check-circle',
        'vue-awesome/icons/cog',
        'vuedraggable',
        'vue-snotify'
    ]) //'laravel-vue-pagination
    .sass('resources/assets/sass/vendor.scss', 'public/css')
    .sass('resources/assets/sass/app.scss', 'public/css');